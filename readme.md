eMenu application:


Preinstallation:

1. git clone git@gitlab.com:pprzybyt/blue_project.git
2. cd blue_project

_____

Launching: 
1. docker-compose build
2. docker-compose up
(if something goes wrong -> blue_project_migration_1 exited with code 1 <- type Ctrl + c and repeat instruction: docker-compose up)

___

Configuration in separate terminal:
1. docker exec -it blue_project_web_1 bash

2. python manage.py makemigrations

3. python manage.py migrate

4. python manage.py createsuperuser
(Testing:  coverage run manage.py test eMenu; coverage report)

(if docker-compose goes properly 2 & 3 steps are redundant)

___ 

Admin:
http://localhost:8000/admin/

App:
http://localhost:8000/eMenu/

___

If any error occurs please contact me via pprzybyt@gmail.com









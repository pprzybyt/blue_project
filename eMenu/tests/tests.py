from django.test import TestCase, Client
from eMenu.models import Dish, MenuCard
from django.utils import timezone
from django.urls import reverse


class DishTestCase(TestCase):
    def setUp(self):
        Dish.objects.create(dish_name="aaa", is_vegetarian=False, price=14.2)

    def test_dish_is_created(self):
        pizza = Dish.objects.get(dish_name="aaa")
        self.assertEqual(pizza.is_vegetarian, False)

    def test_creation_time_from_past(self):
        dish = Dish.objects.order_by('pub_date').first()
        self.assertGreater(timezone.now(), dish.pub_date)
        self.assertEqual(dish.edit_date, dish.pub_date)
        dish.price = 15.7
        dish.save()
        self.assertEqual(dish.price, 15.7)
        self.assertGreater(dish.edit_date, dish.pub_date)


class MenuTestCase(TestCase):
    def setUp(self):
        MenuCard.objects.create(menu_name="menu", menu_description="desc")
        Dish.objects.create(dish_name="dish", is_vegetarian=True, price=12.3)

    def test_menu_after_creation(self):
        menu = MenuCard.objects.get(menu_name="menu")
        dish = Dish.objects.get(dish_name="dish")
        self.assertGreater(MenuCard.objects.all().count(), 0)
        self.assertQuerysetEqual(menu.dishes.all(), [])
        menu.dishes.add(dish)
        self.assertGreater(menu.dishes.all().count(), 0)


class MenuDetailViewTests(TestCase):
    def test_detail_view(self):
        MenuCard.objects.create(menu_name="menu", menu_description="desc")
        menu = MenuCard.objects.get(menu_name="menu")
        client = Client()
        response = client.get(reverse("detail", args=(menu.id,)))
        self.assertEqual(response.status_code, 200)
        max_id = MenuCard.objects.order_by("id").last().id
        response = client.get(reverse("detail", args=(max_id + 1,)))
        self.assertEqual(response.status_code, 404)


class MenuIndexViewTests(TestCase):
    def test_index_view(self):
        client = Client()
        response = client.get(reverse("index"))
        self.assertEqual(response.status_code, 200)


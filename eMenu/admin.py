from django.contrib import admin

from .models import MenuCard, Dish


class DishInline(admin.StackedInline):
    model = MenuCard.dishes.through
    extra = 3


class MenuCardAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['menu_name']}),
        ('Description', {'fields': ['menu_description'], 'classes': ['collapse']}),
    ]
    inlines = [DishInline]


class DishAdmin(admin.ModelAdmin):
    list_display = ('dish_name', 'pub_date', 'edit_date')


admin.site.register(MenuCard, MenuCardAdmin)
admin.site.register(Dish, DishAdmin)

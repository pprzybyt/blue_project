from django.db import models
from django.conf.urls.static import static
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator


class Dish(models.Model):
    dish_name = models.CharField(max_length=100)
    dish_description = models.CharField(max_length=200)
    dish_pic = models.ImageField(upload_to=settings.MEDIA_URL[1:] + "img", default=settings.MEDIA_URL[1:] + "img/no-img.jpg")
    price = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(1000.0)],)
    pub_date = models.DateTimeField('date published', auto_now_add=True)
    edit_date = models.DateTimeField('date edited', auto_now=True)
    is_vegetarian = models.BooleanField()
    preparation_time = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.dish_name

    class Meta:
        verbose_name_plural = "Dishes"


class MenuCard(models.Model):
    menu_name = models.CharField(max_length=100, unique=True)
    menu_description = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', auto_now_add=True)
    edit_date = models.DateTimeField('date edited', auto_now=True)
    dishes = models.ManyToManyField(Dish)

    def __str__(self):
        return self.menu_name

    @property
    def get_dishes_count(self):
        return self.dishes.all().count()

from django.db import migrations
from django.core.management import call_command
import os


def load_initial_data(apps, schema_editor):
    call_command("loaddata", "init_data.json")


def load_superuser(apps, schema_editor):
    call_command("loaddata", "login_info.json")


class Migration(migrations.Migration):

    dependencies = [
        ('eMenu', '0001_initial'),
    ]

    operations = [
        # migrations.RunPython(load_superuser),
        migrations.RunPython(load_initial_data),
    ]

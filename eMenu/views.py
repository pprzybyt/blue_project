from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import MenuCard
from django.utils import timezone
from django.core import serializers


class MenuListView(ListView):
    model = MenuCard
    template_name = 'eMenu/index.html'
    context_object_name = 'menus'
    paginate_by = 3
    # queryset = MenuCard.objects.all()
    ordering = 'id'
    order = "asc"

    def get_queryset(self):
        self.order = self.request.GET.get('order', "asc")
        self.ordering = self.request.GET.get("ordering", 'id')

        new_context = sorted([elem for elem in MenuCard.objects.all() if elem.get_dishes_count > 0],
                             key=lambda m: str(getattr(m, self.ordering)).lower(),
                             reverse=self.order == "desc")
        return new_context

    def get_ordering(self):
        self.order = self.request.GET.get('order', 'asc')
        selected_ordering = self.request.GET.get('ordering', 'id')
        if self.order == "desc":
            selected_ordering = "-" + selected_ordering
        return selected_ordering

    def get_context_data(self, **kwargs):
        context = super(MenuListView, self).get_context_data(**kwargs)
        context['current_order'] = self.get_ordering()
        context['order'] = self.order
        return context


class MenuDetailView(DetailView):
    model = MenuCard
    template_name = 'eMenu/detail.html'
    context_object_name = 'menu'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        # print(self.model.objects.filter(pk=self.kwargs['pk']).values('dishes'))
        # context['data'] = serializers.serialize("python", self.model.objects.filter(pk=self.kwargs['pk']).values('dishes'))
        return context
